package be.kdg.prog4.tdd;

import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class UserManagmenet {
    private Set<User> users;

    public UserManagmenet() {
        users =new HashSet<>();
    }

    public void add(String username, String password){
        this.users.add(new User(username,password));
    }

    public boolean checkLogin(String username, String password) {
        return users.contains(new User(username,password));
    }

    public void removeUser(String username, String password) {
        this.users.remove(new User(username,password));
    }

    public Set<User> getUsers() {
        return users;
    }


}
