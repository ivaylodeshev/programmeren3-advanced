package be.kdg.prog4.tdd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class FavoriteService {

    @Autowired
    private UserManagmenet userManagmenet;
    private List<String> favoriteList = new ArrayList<>();

    public void addUser(String root, String rootpasswd, String username, String password) {
        if ("root".equals(root) && "rootpasswd".equals(rootpasswd)) {
            userManagmenet.add(username, password);
        }
    }

    public void removeUser(String root, String rootpasswd, String username, String password) {
        if ("root".equals(root) && "rootpasswd".equals(rootpasswd)) {
            userManagmenet.removeUser(username, password);
        }
    }

    public boolean checkLogin(String username, String password) {
        if ("root".equals(username) && "rootpasswd".equals(password)) {
           return true;
        }else{
            return userManagmenet.checkLogin(username, password);
        }

    }

    public List<String> getFavorites(String username, String password) {
        List<String> favorites = new ArrayList<>();


        for (User u : userManagmenet.getUsers()) {
            if( u.equals(new User(username,password))){
                List<String>  fa = u.getFavorites();
                 favorites = u.getFavorites();

            }

        }
        return favorites;

    }
    public void addFavorite(String username, String password, String favorite1) {
        // userManagmenet.add(username, password);
        //  User user = new User();
        for (User u : userManagmenet.getUsers()) {
            if (u.equals(new User(username, password))) {
                List<String> favorites = u.getFavorites();
                favorites.add(favorite1);
                u.setFavorites(favorites);

            }



        }
    }
   public void removeFavorite(String username, String password, String favorite1) {
       for (User u : userManagmenet.getUsers()) {
           if (u.equals(new User(username, password))) {
               List<String> favorites = u.getFavorites();
               favorites.remove(favorite1);
               u.setFavorites(favorites);
               System.out.println(u);

           }
           Set<User> user = userManagmenet.getUsers();



       }

    }
}
